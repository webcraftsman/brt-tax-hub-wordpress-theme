function smoothScroll() {
	$('.cta-button').on('click',function(e) {
		e.preventDefault();

		var target = this.hash;
		$target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
		}, 900, 'swing');
	});
}
smoothScroll();

function videoModal(){
	

	$('.resource .video').on('click', function(e){
		var embedCode = $(this).attr('data-video'),	
			videoEmbed = '<div class="video-container"><div class="close-btn">Close<span></span><span></span></div><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + embedCode + '?rel=0&amp;showinfo=0;autoplay=1;" frameborder="0" allowfullscreen></iframe></div></div>',
			modal = '<div id="video-modal">' + videoEmbed  + '</div>';
			
			(e).preventDefault();
			$('body').append(modal);

			$('.close-btn').on('click', function(){
				$('#video-modal').remove();
			});
	});

	
}


function brtResize ( $, window ) {
	window.watchResize = function( callback )
	{
		var resizing;
		function done()
		{
			clearTimeout( resizing );
			resizing = null;
			callback();
		}
		$(window).resize(function(){
			if ( resizing )
			{
				clearTimeout( resizing );
				resizing = null;
			}
			resizing = setTimeout( done, 50 );
		});
		// init
		callback();
	};
	window.watchResize(function(){
		var size = $(window).width();

		if ( size >= 600  && !$('body').hasClass('video-modal-enabled')) {
			$('body').addClass('video-modal-enabled');
			videoModal();
		}

		if (size < 600 && $('body').hasClass('video-modal-enabled'))
		{
			$('body').removeClass('video-modal-enabled');
			$('.video').unbind('click');
		}

	});
}
brtResize( jQuery, window );

var clip = new Clipboard('.copy-link button');

clip.on("success", function() {
  //document.body.insertAdjacentHTML('beforeend', '<div>that worked.</div>');
  alert('Link was copied to your Clipboard');
});
clip.on("error", function() {
  //document.body.insertAdjacentHTML('beforeend', '<div>that didn\'t work.</div>');
  alert('Link was not copied to your Clipboard');
});