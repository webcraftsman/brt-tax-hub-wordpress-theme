<footer role="contentinfo">
	<ul class="primary-links">
		<li><a href="http://businessroundtable.org/sitemap">Sitemap</a></li>
		<li><a href="http://businessroundtable.org/privacy">Privacy Policy</a></li>
		<li><a href="http://businessroundtable.org/terms-conditions">Terms &amp; Conditions</a></li>
		<li><a href="http://businessroundtable.org/rss.xml">RSS</a></li>
	</ul>

	<ul class="social-links">
		<li class="twitter"><a href="https://twitter.com/BizRoundtable" class="twitter">Twitter</a></li>
		<li class="facebook"><a href="https://www.facebook.com/BusinessRoundtable" class="facebook">Facebook</a></li>
		<li class="youtube"><a href="http://www.youtube.com/user/BusinessRoundtable" class="youtube">YouTube</a></li>
		<li class="medium"><a href="https://medium.com/business-roundtable" class="medium-link" target="_blank">Medium</a></li>
	</ul>

</footer>
<?php wp_footer(); ?>