<?php get_header(); ?>

<body>

	<header role="banner">
		<h1>
		<?php $logo = get_field('logo_1x');
			  $logo2x = get_field('logo_2x');
		if ($logo && $logo2x):?>
			<img srcset="<?php the_field('logo_1x'); ?> 1x, <?php the_field('logo_2x'); ?> 2x" src="<?php the_field('logo_1x'); ?>" alt="Business Roundtable: More Than Leaders. Leadership." />
		<?php endif; ?>
		</h1>
		<div class="title-bar">
			<h2><?php the_field('title_bar');?></h2>
			<a class="cta-button" href="#act-now"><?php the_field('cta_button_text');?><i class="icon"></i></a>
		</div>
	</header>
<?php if(get_field('video_embed')): ?>
	<section id="video">
		<div class="video-container">
			<?php the_field('video_embed'); ?>
		</div>
	</section>
<?php endif; ?>
	<section id="thirty-years-later">
		<div class="text-content">
			<h2 class="section-heading"><span>More Than 30 Years Later,</span> <span>Americans Can&rsquo;t Wait</span></h2>
			<p><strong><?php the_field('first_section_lede');?></strong></p>
			<p><?php the_field('first_section_paragraph');?></p>
		</div>
		<figure class="datamoji">
			<img srcset="<?php the_field('first_section_datamoji@1x'); ?> 1x, <?php the_field('first_section_datamoji@2x'); ?> 2x" src="<?php the_field('first_section_datamoji@1x'); ?>" alt="" />
			<div class="sharing-tools">
				<span class="share-datamoji"><em>Share</em><br/>DataMoji</span>
				<ol class="share-links">
					<li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Ftaxreform.brt.org%2Fwp-content%2Fuploads%2F2017%2F10%2Fcorporate-tax-rate.png" target="_blank">Share to Facebook</a></li>
					<li class="twitter"><a href="http://ctt.ec/eMey8" target="_blank">Share on Twitter</a></li>
					<li class="copy-link"><button data-clipboard-text="<?php the_field('first_section_datamoji@1x'); ?>">Copy Link</button></li>
				</ol>
			</div>
		</figure>
	</section>

	<section id="keeping-jobs-at-home">
		<div class="text-content">
			<h2 class="section-heading"><span>Keeping Jobs at Home</h2>
			<p><strong><?php the_field('second_section_lede');?></strong></p>
			<p><?php the_field('second_section_paragraph');?></p>
		</div>
 		<figure class="datamoji">
 			<img srcset="<?php the_field('second_section_datamoji@1x'); ?> 1x, <?php the_field('second_section_datamoji@2x'); ?> 2x" src="<?php the_field('second_section_datamoji@1x'); ?>" alt="" />
 			<div class="sharing-tools">
 				<span class="share-datamoji"><em>Share</em><br/>DataMoji</span>
 				<ol class="share-links">
 					<li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Ftaxreform.brt.org%2Fwp-content%2Fuploads%2F2017%2F10%2Fcompanies-lost.png" target="_blank">Share to Facebook</a></li>
 					<li class="twitter"><a href="http://ctt.ec/mb4ft" target="_blank">Share on Twitter</a></li>
 					<li class="copy-link"><button data-clipboard-text="<?php the_field('second_section_datamoji@1x'); ?>">Copy Link</button></li>
 				</ol>
 			</div>
 		</figure>
	</section>

	<section id="two-million-new-jobs">
		<div class="text-content">
			<h2 class="section-heading"><span>Workers</span><span>Will Benefit</span></h2>
			<p><strong><?php the_field('third_section_lede');?></strong></p>
			<p><?php the_field('third_section_paragraph');?></p>
		</div>
		<figure class="datamoji">
			<img srcset="<?php the_field('third_section_datamoji@1x'); ?> 1x, <?php the_field('third_section_datamoji@2x'); ?> 2x" src="<?php the_field('third_section_datamoji@1x'); ?>" alt="" />
			<div class="sharing-tools">
				<span class="share-datamoji"><em>Share</em><br/>DataMoji</span>
				<ol class="share-links">
					<li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Ftaxreform.brt.org%2Fwp-content%2Fuploads%2F2017%2F10%2Fnew-jobs.png" target="_blank">Share to Facebook</a></li>
					<li class="twitter"><a href="http://ctt.ec/w2OEm" target="_blank">Share on Twitter</a></li>
					<li class="copy-link"><button data-clipboard-text="<?php the_field('third_section_datamoji@1x'); ?>">Copy Link</button></li>
				</ol>
			</div>
		</figure>
	</section>

	<section id="act-now">
		<div class="text-content">
			<h2 class="section-heading"><span>Act Now</span></h2>
			<h3><?php the_field('act_now_subheading');?></h3>
			<p><?php the_field('act_now_paragraph');?></p>
		</div>
		<div class="advocacy">
			<div class="advocacy-actionwidget" data-domain="mobilize4change.org" data-shorturl="8v2u2E2"  style="width: 100%; height: 100%;"></div>
			<script>
			(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = '//mobilize4change.org/js/embed/widget/advocacywidget.min.js';
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'advocacy-actionwidget-code'));
			</script>
		</div>

	</section>

	<section id="more-resources">
		<h2 class="section-heading"><span>More Resources</span></h2>
		<div class="resource-listing">
	<?php
		$args= array(
			'post_type' => 'resources',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page' => -1
		);
		$resources_query = new WP_Query($args);
	?>
	<?php if ($resources_query->have_posts()):?>
		<?php while ( $resources_query->have_posts() ) : $resources_query->the_post();?>
		<?php $type = get_field('resource_type'); ?>
		 <div class="resource">
	 		<?php $resourceImage = get_field('thumbnail'); ?>
	 		<?php if ($resourceImage): ?>
	 		<figure>
	 			<?php if ($type === 'video'):?>
	 				<a href="https://youtu.be/<?php the_field('youtube_url');?>" data-video="<?php the_field('youtube_url');?>" class="video" target="_blank"><i class="icon"></i>
	 			<?php else: ?>
	 				<a href="<?php the_field('link');?>">
	 			<?php endif;?>
	 					<img src="<?php the_field('thumbnail');?>" />
	 				</a>
	 		</figure>
	 		<?php endif; ?>
	 		<h3 class="resource-title"><?php the_title();?></h3>
	 		<?php if ($type === 'video'):?>
				<p class="resource-link"><a href="https://youtu.be/<?php the_field('youtube_url');?>" class="video  class="video" target="_blank"" data-video="<?php the_field('youtube_url');?>">Watch Video</a></p>
			<?php else: ?>
				<p class="resource-link"><a href="<?php the_field('link');?>">Read Report</a></p>
			<?php endif; ?>
		</div>
	<?php endwhile;?>
	<?php endif;?>
</div>

	</section>

	<?php get_footer(); ?>
</body>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write(unescape("%3Cscript src='<?php bloginfo('template_directory'); ?>_js/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
	}
</script>
<script src="<?php bloginfo('template_directory'); ?>/_js/clipboard.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/_js/main.min.js"></script>
</html>