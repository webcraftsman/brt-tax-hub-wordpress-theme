<?php get_header(); ?>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<body>

	<header role="banner">
		<h1>
		<?php $logo = get_field('logo_1x');
			  $logo2x = get_field('logo_2x');
		if ($logo && $logo2x):?>
			<img srcset="<?php the_field('logo_1x'); ?> 1x, <?php the_field('logo_2x'); ?> 2x" src="<?php the_field('logo_1x'); ?>" alt="Business Roundtable: More Than Leaders. Leadership." />
		<?php endif; ?>
		</h1>
		<div class="title-bar">
			<h2><?php the_field('title_bar');?></h2>
			<a class="cta-button" href="#act-now"><?php the_field('cta_button_text');?><i class="icon"></i></a>
		</div>
	</header>
<?php if(get_field('video_embed')): ?>
	<section id="video">
		<div class="video-container">
			<?php the_field('video_embed'); ?>
		</div>
	</section>
<?php endif; ?>

	<section id="new-video-section">
		<h2 class="section-heading smaller-screens"><span>Tax Reform Will Allow</span> <span>Businesses of All Sizes</span> <span>to Invest in America</span></h2>
		<h2 class="section-heading larger-screens"><span>Tax Reform Will Allow Businesses of </span><span>All Sizes to Invest in America</span></h2>
		<div class="video">
			<div class="video-container">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/1A4i_jUOqKs?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<p><a href="https://businessroundtable.org/media/news-releases/business-roundtable-presses-tax-reform-new-national-television-ad-featuring" class="btn" target="_blank">Learn More About Mike and Camcraft</a></p>
		</div>
		<div class="text-content">
			<p>Meet Mike Bertsche, President and CEO of Camcraft, a 300-person manufacturing company that makes engine components for cars, trucks and construction equipment. Based in Hanover Park, Illinois, Camcraft has been in business and a part of the local community for almost 25 years. Camcraft supplies components to Cummins Inc. and several other leading U.S. companies headed by Business Roundtable members.</p>
			<p>Business Roundtable members collectively employ more than 16 million people directly and generate more than $440 billion in revenues annually for small and medium-sized U.S. businesses like Camcraft. Pro-growth tax reform will allow businesses of all sizes to continue to invest in their local communities, hire and increase employee wages.</p>
		</div>

	</section>

	<section id="act-now">
		<div class="text-content">
			<h2 class="section-heading"><span>Act Now</span></h2>
			<h3><?php the_field('act_now_subheading');?></h3>
			<p><?php the_field('act_now_paragraph');?></p>
		</div>
		<div class="advocacy">
			<div class="advocacy-actionwidget" data-domain="mobilize4change.org" data-shorturl="8v2u2E2"  style="width: 100%; height: 100%;"></div>
			<script>
			(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = '//mobilize4change.org/js/embed/widget/advocacywidget.min.js';
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'advocacy-actionwidget-code'));
			</script>
		</div>
	</section>

	<section id="more-resources">
		<h2 class="section-heading"><span>More Resources</span></h2>
		<div class="resource-listing">
	<?php
		$args= array(
			'post_type' => 'resources',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page' => -1
		);
		$resources_query = new WP_Query($args);
	?>
	<?php if ($resources_query->have_posts()):?>
		<?php while ( $resources_query->have_posts() ) : $resources_query->the_post();?>
		<?php $type = get_field('resource_type'); ?>
		 <div class="resource">
	 		<?php $resourceImage = get_field('thumbnail'); ?>
	 		<?php if ($resourceImage): ?>
	 		<figure>
	 			<?php if ($type === 'video'):?>
	 				<a href="https://youtu.be/<?php the_field('youtube_url');?>" data-video="<?php the_field('youtube_url');?>" class="video" target="_blank"><i class="icon"></i>
	 			<?php else: ?>
	 				<a href="<?php the_field('link');?>">
	 			<?php endif;?>
	 					<img src="<?php the_field('thumbnail');?>" />
	 				</a>
	 		</figure>
	 		<?php endif; ?>
	 		<h3 class="resource-title"><?php the_title();?></h3>
	 		<?php if ($type === 'video'):?>
				<p class="resource-link"><a href="https://youtu.be/<?php the_field('youtube_url');?>" class="video  class="video" target="_blank"" data-video="<?php the_field('youtube_url');?>">Watch Video</a></p>
			<?php else: ?>
				<p class="resource-link"><a href="<?php the_field('link');?>">Read Report</a></p>
			<?php endif; ?>
		</div>
	<?php endwhile;?>
	<?php endif;?>
</div>

	</section>

	<?php get_footer(); ?>
</body>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write(unescape("%3Cscript src='<?php bloginfo('template_directory'); ?>_js/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
	}
</script>
<script src="<?php bloginfo('template_directory'); ?>/_js/clipboard.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/_js/main.min.js"></script>
</html>
