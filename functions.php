<?php 
	function my_custom_login_logo() {
		$url = get_field('login_screen_background', 'option');
	    echo '<style type="text/css">
	        .login h1 a { background-image:url(http://taxreform.brt.org/wp-content/uploads/2017/10/logo@2x.jpg) !important; background-position: 0 0 !important; background-size:322px 44px !important; width:322px !important; height:44px !important;}
	        #loginform { background: #eee !important;}
	        body {background: #fff !important;}
	        #nav a, #backtoblog a { color:#E94B35 !important;}
	        #nav a:hover, #backtoblog a:hover { color:#E94B35 !important; text-decoration: underline !important;}
	        .wp-core-ui .button-primary { background:#E94B35 !important; text-shadow: none !important; box-shadow: none !important; -webkit-box-shadow: none !important; border-color:#E94B35 !important;}
	        .login #login_error, .login .message {border-left-color:#E94B35 !important;}
	    </style>';
	}
	add_action('login_head', 'my_custom_login_logo');

 	function remove_menus(){
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'edit-comments.php' );
	}
	add_action( 'admin_menu', 'remove_menus' );
	
	if( function_exists('acf_add_options_page') ) {
	
		acf_add_options_page();
		
	}
?>