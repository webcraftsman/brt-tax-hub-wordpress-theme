<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/_img/favicon.ico" />
	<title>Business Roundtable | Why Tax Reform Matters</title>
	<link rel="stylesheet" href="https://use.typekit.net/zrn7opa.css">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/_css/main.css">

	<meta property="og:type" content="article" /> 
	<meta property="og:title" content="Business Roundtable: Why Tax Reform Matters" />
	<meta property="og:description" content="It's been more than three decades since America reformed its tax system. As countries around the world became more competitive, our tax system remained the same, putting American businesses at a disadvantage. Now is the time to update our broken tax system so we can invest in America, and jump-start our economy." />
	<meta property="og:image" content="http://taxreform.brt.org/wp-content/uploads/2017/10/corporate-tax-rate.png" />
	<meta property="og:url" content="http://taxreform.brt.org" />
	<meta property="og:site_name" content="Why Tax Reform Matters" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="Business Roundtable: Why Tax Reform Matters">
	<meta name="twitter:description" content="It's been more than three decades since America reformed its tax system. As countries around the world became more competitive, our tax system remained the same, putting American businesses at a disadvantage. Now is the time to update our broken tax system so we can invest in America, and jump-start our economy.">
	<meta name="twitter:image" content="http://taxreform.brt.org/wp-content/uploads/2017/10/corporate-tax-rate.png">
	<meta name="twitter:site" content="@BizRoundtable">
	<meta name="twitter:creator" content="@BizRoundtable">

</head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5818203-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-5818203-13');
</script>